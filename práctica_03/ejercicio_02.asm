        processor 16f877 ;Indica la versi�n de procesador
        include <p16f877.inc> ;Incluye la librer�a de la versi�n del procesador
        org 0H ;Carga al vector de RESET la direcci�n de inicio
        goto inicio
        org 05H ;Direcci�n de inicio del programa del usuario
;Se asigna una etiqueta a las direcciones de memoria que servir�n para
;seleccionar el "suprograma" que se ejecutar�
n0      equ h'24';H'00'
n1      equ h'25';H'01'
n2      equ h'26';H'02'
n3      equ h'27';H'03'
n4      equ h'29';H'04'
n5      equ h'30';H'05'
;Etiquetas pasa almacenar los valores con los que se realizara el retardo
valor1  equ h'21'
valor2  equ h'22'
valor3  equ h'23'

inicio:
        CLRF PORTA
        BSF STATUS,RP0 ;Cambia la banco 1
        BCF STATUS,RP1
        MOVLW 06H ;Configura puertos A y E como digitales
        MOVWF ADCON1
        MOVLW d'1' ;Configura el puerto A como entrada
        MOVWF TRISA
        MOVLW H'0' ;Se carga un 0 en el registro de trabajo
        MOVWF TRISB ;Se mueve el 0 al registro TRISB para configurarlo como salida
        clrf PORTB
        BCF STATUS,RP0 ;Regresa al banco cero
        ;Cargando datos para la seleccion
        MOVLW H'0'
        MOVWF n0
        MOVLW H'1'
        MOVWF n1
        MOVLW H'2'
        MOVWF n2
        MOVLW H'3'
        MOVWF n3
        MOVLW H'4'
        MOVWF n4
        MOVLW H'5'
        MOVWF n5
main:
        movfw PORTA     ;Se carga la entrada del puerto A al registro de trabajo
        xorwf n0,w      ;Se comprueba si la seleccion es H'00'
        btfsc STATUS,Z  ;Si la bandera z se mantiene en 0 significa que todos
                        ;los bits fueron iguales ya que 1 or 1=0
        goto apagado

        movfw PORTA     ;Se carga la entrada del puerto A al registro de trabajo
        xorwf n1,w      ;Se comprueba si la seleccion es H'01'
        btfsc STATUS,Z  ;Si la bandera z se mantiene en 0 significa que todos
                        ;los bits fueron iguales ya que 1 or 1=0
        goto encendido

        movfw PORTA     ;Se carga la entrada del puerto A al registro de trabajo
        xorwf n2,w      ;Se comprueba si la seleccion es H'02'
        btfsc STATUS,Z  ;Si la bandera z se mantiene en 0 significa que todos
                        ;los bits fueron iguales ya que 1 or 1=0
        goto corrimiento_der

        movfw PORTA     ;Se carga la entrada del puerto A al registro de trabajo
        xorwf n3,w      ;Se comprueba si la seleccion es H'03'
        btfsc STATUS,Z  ;Si la bandera z se mantiene en 0 significa que todos
                        ;los bits fueron iguales ya que 1 or 1=0
        goto corrimiento_izq

        movfw PORTA     ;Se carga la entrada del puerto A al registro de trabajo
        xorwf n4,w      ;Se comprueba si la seleccion es H'04'
        btfsc STATUS,Z  ;Si la bandera z se mantiene en 0 significa que todos
                        ;los bits fueron iguales ya que 1 or 1=0
        goto corrimiento_der_izq

        movfw PORTA     ;Se carga la entrada del puerto A al registro de trabajo
        xorwf n5,w      ;Se comprueba si la seleccion es H'05'
        btfsc STATUS,Z  ;Si la bandera z se mantiene en 0 significa que todos
                        ;los bits fueron iguales ya que 1 or 1=0
        goto encendido_apagado
        goto main
apagado:
        movlw h'00'
        movwf PORTB ;Se asigna todos los bits apagados al puerto de salida
        goto main
encendido:
        movlw h'FF'
        movwf PORTB ;Se asigna todos los bits encendidos al puerto de salida
        goto main
corrimiento_der:
        movlw h'80' ;Se carga la cadena de bits b'1000 0000'
        movwf PORTB ;Se carga la cadena b'1000 0000' al puerto de salida
        call retardo
corrimiento_der_loop:
        rrf PORTB,1 ;Se realiza un corrimiento a la derecha en el puerto B
        call retardo ;Retardo de medio segundo
        btfss PORTB,0 ;Si el 1 de la '1000 0000' ha llegado al bit 0 del regitro
                    ;Se vuelve a cargar la cadena de bits al puerto B
                    ;Sino se continua con el corrimiento
        goto corrimiento_der_loop
        goto main
corrimiento_izq:
        movlw h'01' ;Se carga la cadena b'0000 0001' al puerto B
        movwf PORTB
        call retardo
corrimiento_izq_loop:
        rlf PORTB,1  ;Se realiza un corrimiento a la izquierda en el puerto B
        call retardo
        btfss PORTB,7 ;Si el 1 de la '0000 0001' ha llegado al �ltomo bit
                    ;del registro Se vuelve a cargar la cadena de bits
                    ;al puerto B, ssino se continua con el corrimiento
        goto corrimiento_izq_loop
        goto main
corrimiento_der_izq:
        movlw h'80' ;Se carga la cadena de bits b'1000 0000'
        movwf PORTB
        call retardo
corrimiento_der_izq_loop_der:
        rrf PORTB,1 ;Se realiza un corrimiento a la derecha en el puerto B
        call retardo
        btfss PORTB,0 ;Si el 1 de la cadena de bits b'1000 0000' llega al primer
                    ;bit del puerto B se cambia por corrimiento a la izquierda
                    ;sino scontinua con el corrimiento a la derecha
        goto corrimiento_der_izq_loop_der
        goto corrimiento_der_izq_loop_izq
corrimiento_der_izq_loop_izq:
        rlf PORTB,1 ;Se realiza un corrimiento a la izquierda en el puerto B
        call retardo
        btfss PORTB,7 ;Si el 1 de la '0000 0001' ha llegado al �ltomo bit
                    ;del registro Se vuelve a cargar la cadena de bits
                    ;al puerto B para volver a realizar corrimineto a la derecha
                    ;sino se continua con el corrimiento
        goto corrimiento_der_izq_loop_izq
        goto main

encendido_apagado
        movlw h'00'
        movwf PORTB ;Se carga la cadena b'0000 0000' al puerto de salida
        call retardo
        movlw h'FF' ;Se carga la cadena b'1111 1111' al puerto de salida
        movwf PORTB
        call retardo
        goto main
retardo:
        ;Se carga el valor 1 como 13
        movlw d'13'
        movwf valor1
tres
    ;Se carga el valor 2 como 255 (valor m�ximo de un registro de 8 bits)
        movwf d'255'
        movwf valor2
dos
    ;Se carga el valor 3 como 255 (valor m�ximo de un registro de 8 bits)
        movlw d'255'
        movwf valor3
uno
        ;Se ejecuta el decremento 255 (valor 1)
        ;durante 255 veces por el decremento de valor 2
        ;por 13 veces con el decremento de valor 3
        ;De esa manera se ejecutan las 3 instrucciones 845325 veces
        ;Por la duraci�n de cada instruccion de 200e-9 segundos
        ;Provancando un retardo de 255*255*13*3*200e-9=0.50 [S]
        decfsz valor3
        goto uno
        decfsz valor2
        goto dos
        decfsz valor1
        goto tres
        return
end
