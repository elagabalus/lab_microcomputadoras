        processor 16f877 ;Indica la versi�n de procesador
        include <p16f877.inc> ;Incluye la librer�a de la versi�n del procesador
        org 0H ;Carga al vector de RESET la direcci�n de inicio
        goto inicio
        org 05H ;Direcci�n de inicio del programa del usuario
inicio:
        CLRF PORTA
        BSF STATUS,RP0 ;Cambia la banco 1
        BCF STATUS,RP1
        MOVLW 06H ;Configura puertos A y E como digitales
        MOVWF ADCON1
        MOVLW d'1' ;Configura el puerto A como entrada
        MOVWF TRISA
        MOVLW H'0' ;Se carga un 0 en el registro de trabajo
        ;Se mueve el 0 al registro TRISB para configurarlo como salida
        MOVWF TRISB
        ;Se elminan todos los dato del puerto B
        clrf PORTB
        BCF STATUS,RP0 ;Regresa al banco cero
main:
        ;Se carga el un 1 en cada bit del registro de trabajo
        MOVLW H'FF'
        ;Se comprueba si el puerto tiene un 1 en el bit 0
        ;Ser� el valor por defecto
        BTFSS PORTA,0
        ;Si no hab�a un 1 se carga un 0 a cada bit del registro
        MOVLW d'0'
        ;Se carga el valor por defecto d'256' o 0 si no hab�a un 1
        MOVWF PORTB
        ;Se vuelve a ejecutar el programa
        GOTO main

end
