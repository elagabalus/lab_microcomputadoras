            processor 16f877
            include<p16f877.inc>
            ;Se definen direcciones de las variables
            valor1 equ h'21'
            valor2 equ h'22'
            valor3 equ h'23'
            ;Se definen constantes
            cte1 equ 30h ;Ahora se retrasa durante 1s
            cte2 equ 50h
            cte3 equ 60h
            ;Se carga el vector de inicio
            org 0
            goto inicio
            org 5

inicio
            ;Se selecciona el banco 1 (01) ya que ah� est� el registro trisB
            ;Se enciende el bit 5 del registro estatus
            bsf STATUS,5
            ;Se apaga el bit 6 del registro estatus
            BCF STATUS,6
            ;Se carga un 0 en el registro de trabajo
            MOVLW H'0'
            ;Se mueve el 0 al registro TRISB para configurarlo como salida
            MOVWF TRISB
            ;Se selecciona el banco o
            ;Se apaga el bit 5 del registro STATUS
            BCF STATUS,5
            ;Se ponen a 0 todos los bits del puerto B
            clrf PORTB
loop2       
            MOVLW d'255' ;Equivalente decimal a '11111111'b
            MOVWF PORTB  ;Se encienden todas bits del puerto
            ;call retardo ;Se ejecutan instrucciones durante 1s
            MOVLW d'0'   ;Se carga el numero 0
            ;Se mueve el 0 al puerto b pagando todos los bits
            MOVWF PORTB  
            ;call retardo ;Se ejecutan instrucciones durante 1s
            goto loop2

retardo     movlw cte1 ;Se carga la constante 20h al registro de trabajo
            ;Se mueve la constante al registro de la variable valor1
            movwf valor1
tres        movlw cte2 ;Se carga la constante 50h al registro de trabajo
            ;Se mueve la constante al registro de la variable valor2
            movwf valor2
dos         movlw cte3 ;Se carga la constante 60h al registro de trabajo
            ;Se mueve la constante al registro de la variable valor3
            movwf valor3
            ;se decrementa el valor 3 hasta llegar a cero
            ;Es el contador m�s anidado
uno         decfsz valor3
            goto uno
            ;se decrementa el valor 2 hasta llegar a cero
            ;Es el contador segundo m�s anidado
            decfsz valor2
            goto dos
            ;se decrementa el valor 1 hasta llegar a cero
            ;Es el ciclo en el que se anidan los dem�s ciclos
            decfsz valor1
            goto tres
            return
            END