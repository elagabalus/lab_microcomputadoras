            ;Indica la versi�n del procesador
            processor 16f877
            ;Librer�a de la versi�n del procesador
            include<p16f877.inc>
            ;Se definen variables
            contador equ h'20'
            valor1 equ h'21'
            valor2 equ h'22'
            valor3 equ h'23'
            ;Se definen constantes para generar el retardo
            cte1 equ 20h
            cte2 equ 50h
            cte3 equ 60h

            org 0
            goto inicio
            org 5
inicio
            ;Se selecciona el banco 1 (01) ya que ah� est� el registro trisB
            ;Se enciende el bit 5 del registro estatus
            bsf STATUS,5
            ;Se apaga el bit 6 del registro estatus
            BCF STATUS,6
            ;Se carga un 0 en el registro de trabajo
            MOVLW H'0'
            ;Se mueve el 0 al registro TRISB para configurarlo como salida
            MOVWF TRISB
            ;Se selecciona el banco o
            ;Se apaga el bit 5 del registro STATUS
            BCF STATUS,5
            ;Se ponen a 0 todos los bits del puerto B
            clrf PORTB
loop2
            ;Se enciende el primer bit del puerto B
            bsf PORTB,0
            ;Se llama a la funci�n de retardo
            ;call retardo
            ;Se apaga el primer bit del puerto B
            bcf PORTB,0
            ;Se llama a la funci�n de retardo
            ;call retardo
            ;Se repite el encendido y apagado
            goto loop2
retardo     movlw cte1 ;Se carga la constante 20h al registro de trabajo
            ;Se mueve la constante al registro de la variable valor1
            movwf valor1
tres        movlw cte2 ;Se carga la constante 50h al registro de trabajo
            ;Se mueve la constante al registro de la variable valor2
            movwf valor2
dos         movlw cte3 ;Se carga la constante 60h al registro de trabajo
            ;Se mueve la constante al registro de la variable valor3
            movwf valor3
            ;se decrementa el valor 3 hasta llegar a cero
            ;Es el contador m�s anidado
uno         decfsz valor3
            goto uno
            ;se decrementa el valor 2 hasta llegar a cero
            ;Es el contador segundo m�s anidado
            decfsz valor2
            goto dos
            ;se decrementa el valor 1 hasta llegar a cero
            ;Es el ciclo en el que se anidan los dem�s ciclos
            decfsz valor1
            goto tres
            return
            END
