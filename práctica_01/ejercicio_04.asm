        processor 16f877
        include <p16f877.inc>
        ;Declaramos las siguiente variables
        ;Se asigna la dirección de memoria 26 a la variable salida
salida  equ H'26'
        ;Se asigna la dirección de memoria 27 a la variable N
        ;que funcionará como contador
N       equ H'27'
        ;Se reincia el microprocesador
        org 0H
        ;Se hace un salto a la rutina de incio
        goto inicio
        org 05H
inicio: 
        ;Se carga la literal 20 en decimal a la memoria de trabajo
        MOVLW d'20'
        ;Se mueve el contenido de la memoria de trabajo al regitro N
        ;colocando el número 20 en la variable N
        MOVWF N
        ;Se carga la literal 0, primer elemento de la secuencia a la
        ;memoria de trabajo
        MOVLW d'0'
        ;Se mueve el contenido de la memoria de trabajo el 0
        ;al registro de salida en donde visualizaremos la secuencia
        MOVWF salida
ciclo:
        ;Se incrementa el contenido de la variable de salida
        ;es decir la secuencia
        INCF salida,1
        ;Se decrementa el contador para usar la bandera Z
        ;en cuanto se active
        DECF N;
        ;Se comprueba si la bandera Z está encendida
        btfss STATUS,Z
        ;Si no está encendida se continua con el ciclo
        ;que genera la secuencia
        goto ciclo
        ;Se reinicia la secuencia ejecutando las instrucciones de
        ;la rutina de inicio
        goto inicio
        end; Fin del programa