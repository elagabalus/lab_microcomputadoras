        processor 16f877
        include <p16f877.inc>
        ;Se asigna la direccion 26 de memoria a la variable J
        ;Primer operando
J       equ H'26'
        ;Se asigna la direccion 27 de memoria a la variable K
        ;Primer operando
K       equ H'27'
        ;Se asigna la direccion 28 de memoria a la variable R1
        ;Resultado
R1      equ H'28'
        ;Se asigna la direccion 29 de memoria a la variable C1
        ;Acarreo
C1      equ H'29'
        org 0H
        goto inicio
        org 05H
        ;Función de inicio
inicio:
        ;Se carga el contenido del registro J al registro de trabajo
        movf J,0
        ;Se suma el contenido del registro K a la memoria de trabajo
        addwf K,0
        ;Se traslada el contenido del registro de trabajo a la direccción
        ;de memoria del resultado
        movwf R1
        ;Se limpia el registro de acarreo
        CLRF C1
        ;Se comprueba si la bandera de carry esta encendida
        BTFSS STATUS,C
        ;Si no está no se avanza a poner 1 en el registro de acarreo y se
        ;vuelve a la seccion de inicio
        goto inicio
        ;Esta sección pone un 1 en el registro de acarreo
pon_uno:
        ;Se carga un 1 al registro de trabajo
        MOVLW H'1'
        ;Se mueve el contenido del registro de trabajo al registro de acarreo
        MOVWF C1
        ;Regresamos a la sección de inicio
        goto inicio
        end