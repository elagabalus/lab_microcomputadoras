
        processor 16f877
        include <p16f877.inc>
        ;Se establece el registro en el que se observará la salida
salida  equ H'26'
        ;Se crea una variable de contador en la direccion
        ;de memoria 26
N       equ H'27'
        ;Reseteamos el microprocesador
        org 0H
        ;vamos a la rutina de inicio
        goto inicio
        org 05H
        ;Comienza la rutina
inicio:
        ;Se carga una literal a la memoria de trabajo
        ;Este número se usa como un contador que se decrementa
        ;a cada iteraación para que al llegar a cero y se active
        ;la bandera z ésta se use como condicion para repetir el ciclo
        MOVLW H'07'
        ;se carga la literal a la direccion de memoria de la variable N
        MOVWF N
        ;Se carga el número 1 al registro de trabajo, ya que en binario
        ;es el 1 (primer elemento de la secuencia) y al realizar un
        ;corrimiento a la izquierda sirve para representar a todos los
        ;demás elementos
        MOVLW H'01'
        ;Se coloca el contenido de la memoria de trabajo en la dirección
        ;de la variable salida (en la que observaremos la secuencia)
        MOVWF salida
ciclo:
        ;Se realiza un corrimiento a la izquierda y se vielve a almacenar
        ;en la direccion de memoria de la varible salida
        rlf salida,1
        ;Se decrementa el contenido del registro N
        DECF N;
        ;Se comprueba si la bandera Z está encendida
        btfss STATUS,Z
        ;Si no está encendida se continúa con el ciclo
        goto ciclo
        ;Si está encendida se detiene el ciclo y volvemos a inicio
        goto inicio
        end