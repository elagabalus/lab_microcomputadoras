;Laboratorio de microcomputadoras
;Profesora: Ing. Lourdes Ang�lica Qui�ones Juarez
;Pr�ctica #1
;Programa #1    
        processor 16f877
        include <p16f877.inc>
        ;Declaramos las siguiente variables
        ;A la variable K le saignamos la direccion 26 de nuestra memoria
K       equ H'26'
        ;A la variable L le saignamos la direccion 27 de nuestra memoria
L       equ H'27' 
        ;Reseteamos el mircoprocesador y emepezamos un nuevo programa
        org 0
        goto inicio ;Vamos a la funci�n de inicio
        org 5
inicio: 
        ;Se carga una literal (un n�mero) al registro de trabajo
        movlw h'05'
        ;Se suma el contenido del registro k y 
        ;se guarda en el registro de trabajo
        addwf K,0
        ;Se guarda el contenido del registro de trabajo 
        ;en la direccion de la variable L
        movwf L
        goto inicio ; Regresamos a la funci�n de inicio
        end ; Fin del programa