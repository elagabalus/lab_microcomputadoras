        processor 16f877
        include <p16f877.inc>
N1      equ H'25'
N2      equ H'26'
MEN		equ H'27'
        org 0H
        goto inicio
inicio:
        org 5H
        movf N1,0
        SUBWF N2,0
        BTFSC STATUS,C
        MOVF N1,0
        ;C=1 significa n2-n1>=0
        ;=>n1<n2
        ;=> N1 se asgina como el menor
        BTFSC STATUS,C
        goto asigna
        ;Como se encontro el menor se asigna
        ;Sino se asigna el otro
        MOVF N2,0
asigna:
        MOVWF MEN
        goto inicio
        end
