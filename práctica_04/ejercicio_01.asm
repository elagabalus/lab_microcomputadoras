processor 16f877
include<p16f877.inc>

;Tiempo del retardo y direcciones de memoria
valor1  equ h'21'
valor2  equ h'22'
valor3  equ h'23'
ret1    equ d'13'
ret2    equ d'256'
ret3    equ d'256'

;Direccion de memoria auxiliar y cadenas de entrada
;Para el control de los motores

aux equ h'24'

c0  equ 0h
c1  equ 2h
c2  equ 4h
c3  equ 8h
c4  equ 10h

    org 0h
    goto INICIO
    org 05h

INICIO:
    ;CONFIGURACI�N PUERTO B COMO SALIDA
    bsf STATUS,RP0   ;Se selecciona el banco 1, poniendo el bit 5 (RP0) en 1
    bcf STATUS,RP1   ;y poninendo el bit 6 (RP1) en 0
    movlw H'0'       ;Se carga un cero al registro de trabajo
    movwf TRISB      ;Se configura el puerto B como salida
    clrf PORTB       ;Se ponen en cero todos los bits del puerto B
    ;CONFIGURACI�N PUERTO A COMO ENTRADA
    movlw 06h        ;Configura puertos A y E como digitales
    movwf ADCON1
    movlw 3fh        ;Configura el puerto A como entrada
    movwf TRISA
    clrf PORTA       ;Todos los bits del puerto A a 0

    bcf STATUS,RP0    ;Se vuelve a selecionar el banco 0
MAIN:
        ;PARO
        movlw c0        ;Se carga la cadena de bits para realizar PARO
        movwf aux       ;Se carga en una direcci�n de memoria auxiliar
        movfw PORTA     ;Se carga el valor de entrada en el puerto A
                        ;a la memoria de trabajo
        xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
        btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la funci�n
        goto PARO       ;Se ejecuta la funcion que da la salida de PARO

        ;HORARIO
        movlw c1        ;Se carga la cadena de bits para realizar HORARIO
        movwf aux       ;Se carga en una direcci�n de memoria auxiliar
        movfw PORTA     ;Se carga el valor de entrada en el puerto A
                        ;a la memoria de trabajo
        xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
        btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la funci�n
        goto HORARIO    ;Se ejecuta la funcion que da la salida de HORARIO

        ;ANTIHORARIO
        movlw c2        ;Se carga la cadena de bits para realizar ANTIHORARIO
        movwf aux       ;Se carga en una direcci�n de memoria auxiliar
        movfw PORTA     ;Se carga el valor de entrada en el puerto A
                        ;a la memoria de trabajo
        xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
        btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la funci�n
        goto ANTIHORARIO ;Se ejecuta la funcion que da la salida de ANTIHORARIO

        ;HORARIO1
        movlw c3        ;Se carga la cadena de bits para realizar ANTIHORARIO
        movwf aux       ;Se carga en una direcci�n de memoria auxiliar
        movfw PORTA     ;Se carga el valor de entrada en el puerto A
                        ;a la memoria de trabajo
        xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
        btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la funci�n
        goto  HORARIO1

        ;ANTIHORARIO1
        movlw c4        ;Se carga la cadena de bits para realizar ANTIHORARIO
        movwf aux       ;Se carga en una direcci�n de memoria auxiliar
        movfw PORTA     ;Se carga el valor de entrada en el puerto A
                        ;a la memoria de trabajo
        xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
        btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la funci�n
        goto ANTIHORARIO1

PARO:
       movlw h'00'      ;MOTOR IZQ=OFF DER=OFF
       movwf PORTB
       goto MAIN

HORARIO:
        movlw b'0010'   ;MOTOR IZQ=OFF DER=ON DIR1=1
        movwf PORTB
        call retardo
        goto MAIN

ANTIHORARIO:
        movlw b'0001'    ;MOTOR IZQ=OFF DER=ON DIR1=0
        movwf PORTB
        call retardo
        goto MAIN

HORARIO1:
       movlw b'1000'     ;MOTOR IZQ=ON DER=OFF DIR2=1
       movwf PORTB
       call retardo
       goto MAIN

ANTIHORARIO1:
       movlw b'0100'    ;MOTOR IZQ=ON DER=OFF DIR2=0
       movwf PORTB
       call retardo
       goto MAIN

retardo
    ;Rutina que genera un retardo
    movlw ret1
    movwf valor1
tres
    movwf valor2
    movwf ret2
dos
    movlw ret3
    movwf valor3
uno
    decfsz valor3
    goto uno
    decfsz valor2
    goto dos
    decfsz valor1
    goto tres
    return
end