    processor 16f877
    include<p16f877.inc>

;Variable para la secuencia de retardo
valor1    equ h'20'

org 0h
goto INICIO
org 05h

INICIO:
    clrf PORTA      ;Limpia el puerto A
    clrf PORTB      ;Limpia el puerto B

    bsf STATUS,RP0
    bcf STATUS,RP1  ;Cambio la banco 1
    movlw 00h
    movwf ADCON1    ;Configura puerto A y E como analógicos
    
    movlw 3fh
    movwf TRISA       ;Configura el puerto A como entrada
    movlw h'0'
    movwf TRISB       ;Configura puerto B como salida
    
    bcf STATUS,RP0    ;Regresa al banco 0
    movlw b'11000001'   ;Configuración ADCON0 (Canal 0)
    ;movlw b'11001001'   ;Configuración ADCON0 (Canal 1)
    ;movlw b'11010001'   ;Configuración ADCON0 (Canal 2)
    movwf ADCON0        ;ADCS1=1 ADCS0=1 (Seleccion de frecuencias)
                        ;CHS2=0 CHS1=0 CHS0=0 (Seleccion de pin de entrada)
                        ;GO/DONE=0 ADON=1
MAIN:
    bsf ADCON0,2      ;Se incia la conversión GO=1
    call retardo      ;Retardo para espera la conversión
ESPERA: 
    btfsc ADCON0,2    ;Comprueba que haya terminado la conversion
    goto ESPERA       ;Sino, Vuelve a comprobar que haya terminado la conversion

    movf ADRESH,0     ;Carga el voltaje de entrada (Ve) , convertido a digital
                      ;Al registro de trabajo
                    ;Vcc=5V o 255 digital    
    sublw d'85'     ;Opera, (1/3)Vcc-Ve {dado que Vcc*1/3=(255*1/3)=85}
    btfsc STATUS,0  ;Comprueba si Ve>(1/3)vcc (C=0 negativo)
    goto BAJO       ;(Si, saltar) Ve BAJO (Ve < 1/3Vcc)
    movf ADRESH,0   ;continuar con las comprobaciones

    sublw d'170'    ;Opera, (2/3)Vcc-Ve {dado que Vcc*2/3=(255*1/3)=170}
    btfsc STATUS,0  ;Comprueba si Ve>(2/3)vcc (C=0 negativo)
    goto MEDIO      ;(Si, saltar) Ve MEDIO ((1/3Vcc<Ve<2/3Vcc))
    movf ADRESH,0     ;(No, continuar con las comprobaciones)
    sublw d'255'      ;Opera, Vcc-Ve {dado que Vcc=255}
    btfsc STATUS,0   ;Comprueba si Ve>vcc (C=0 negativo)
    goto ALTO      ;SI, Ve ALTO (2/3Vcc<Ve<Vcc)

BAJO
    ;Voltaje bajo 001
    movlw h'1'
    movwf PORTB
    goto MAIN
MEDIO
    ;Voltaje medio 011
    movlw h'3'
    movwf PORTB
    goto MAIN
ALTO
    ;Volraje alto 111
    movlw h'7'        
    movwf PORTB
    goto MAIN

retardo
;Retardo de 20us
;34*3*200ns=20.4us
    movlw d'34'
    movwf valor1
loop
    decfsz valor1
    goto loop
    return
end