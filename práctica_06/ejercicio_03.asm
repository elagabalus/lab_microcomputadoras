    processor 16f877
    include<p16f877.inc>

;Variable para la secuencia de retardo
valor1 equ h'20'

;Direcciones para almacenar los voltajes
;codficados a digital
ve1 equ h'21'
ve2 equ h'22'
ve3 equ h'23'


org 0h
goto INICIO
org 05h

INICIO:
    clrf PORTA      ;Limpia el puerto A
    clrf PORTB      ;Limpia el puerto B

    bsf STATUS,RP0
    bcf STATUS,RP1  ;Cambio la banco 1
    movlw 00h
    movwf ADCON1    ;Configura puerto A y E como analógicos
    
    movlw 3fh
    movwf TRISA       ;Configura el puerto A como entrada
    movlw h'0'
    movwf TRISB       ;Configura puerto B como salida
    
    bcf STATUS,RP0    ;Regresa al banco 0

MAIN:
    ;LECTURA VOLTAJE 1
    movlw b'11000001'   ;Configuración ADCON0 (Pin A0)
    movwf ADCON0        ;ADCS1=1 ADCS0=1
                        ;CHS2=0 CHS1=0 CHS0=0 (Seleccion del pin A0)
                        ;GO/DONE=0 - ADON=1
    bsf ADCON0,2      ;Iniciar conversion a digital GO=1
    call retardo      ;Retardo para esperar la conversion

VOLTAJE_UNO
    btfsc ADCON0,2    ;Comprueba finalización de la conversion
    goto VOLTAJE_UNO  ;Si no ha terminado vuleve a comprobar
    
    movf ADRESH,0     ;Se carga el voltaje en digital al registro W
    movwf ve1         ;Se almacena la lectura del voltaje 1
    
    ;LECTURA VOLTAJE 2
    movlw b'11001001'   ;Configuración ADCON0 (Pin A1)
    movwf ADCON0        ;ADCS1=1 ADCS0=1
                        ;CHS2=0 CHS1=0 CHS0=1 (Seleccion del pin A1)
                        ;GO/DONE=0 - ADON=1
    bsf ADCON0,2      ;Iniciar conversion a digital GO=1
    call retardo      ;Retardo para esperar la conversion

VOLTAJE_DOS:
    btfsc ADCON0,2    ;Comprueba finalización de la conversion
    goto VOLTAJE_DOS  ;Si no ha terminado vuleve a comprobar
    movf ADRESH,0     ;Se carga el voltaje en digital al registro W
    movwf ve2         ;Se almacena la lectura del voltaje 2

    ;LECTURA VOLTAJE 3
    ;LECTURA VOLTAJE 2
    movlw b'11010001'   ;Configuración ADCON0 (Pin A2)
    movwf ADCON0        ;ADCS1=1 ADCS0=1
                        ;CHS2=0 CHS1=1 CHS0=0 (Seleccion del pin A2)
                        ;GO/DONE=0 - ADON=1
    bsf ADCON0,2      ;Iniciar conversion a digital GO=1
    call retardo      ;Retardo para esperar la conversion

VOLTAJE_TRES:
    btfsc ADCON0,2    ;Comprueba finalización de la conversion
    goto VOLTAJE_TRES ;Si no ha terminado vuleve a comprobar
    movf ADRESH,0     ;Se carga el voltaje en digital al registro W
    movwf ve3         ;Se almacena la lectura del voltaje 2
    
    ;COMPROBACION
    movf ve1,w        ;Se carga el voltaje 1 al registro W
    subwf ve2,w       ;Opera ve2-ve1
    btfss STATUS,0    ;Verifica si ve2>ve1, (positivo C=1)
    goto COMPROBAR_V1      ;No, entonces Ve1>Ve2
    goto COMPROBAR_V2      ;Si, entonces Ve2>Ve1
    

COMPROBAR_V1
    movf ve1,w        ;Se carga el voltaje 1 al registro W
    subwf ve3,w       ;opera ve3-ve1
    btfss STATUS,0    ;Verifica si ve3>ve1
    goto LETRERO_1      ;No, entonces Ve1>Ve2
    goto LETRERO_3      ;Si, entonces Ve3>Ve1
    

COMPROBAR_V2
    movf ve2,w        ;Se carga el voltaje 2 al registro W
    subwf ve3,w       ;opera ve3-ve2
    btfss STATUS,0    ;Verifica si ve3>ve1
    goto LETRERO_2    ;No, entonces Ve2>Ve3
    goto LETRERO_3    ;Si, entonces Ve3>Ve2
    

LETRERO_1:
    ;Voltaje v1 es el mayor (0001)
    movlw h'1'        
    movwf PORTB       
    goto MAIN

LETRERO_2:
    ;Voltaje v2 es el mayor (0011)
    movlw h'3'
    movwf PORTB
    goto MAIN

LETRERO_3:
    ;Voltaje v2 es el mayor (0111)
    movlw h'7'
    movwf PORTB
    goto MAIN

retardo
;Retardo de 20us
;34*3*200ns=20.4us
    movlw d'34'
    movwf valor1
loop
    decfsz valor1
    goto loop
    return
end