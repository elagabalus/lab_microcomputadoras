   processor 16f877
    include<p16f877.inc>

;Variable para la rutina de retardo
valor1 equ H'20'

;Definición de variables a utilizar para
;comparar las entradas a través del puerto A
org 0h
goto INICIO
org 05h

INICIO:
    clrf PORTA      ;Limpia el puerto A
    clrf PORTB      ;Limpia el puerto B

    bsf STATUS,RP0
    bcf STATUS,RP1  ;Cambio la banco 1
    movlw 00h
    movwf ADCON1    ;Configura puerto A y E como analógicos
    
    movlw 3fh
    movwf TRISA       ;Configura el puerto A como entrada
    movlw h'0'
    movwf TRISB       ;Configura puerto B como salida
    
    bcf STATUS,RP0    ;Regresa al banco 0
    movlw b'11000001'   ;Configuración ADCON0
    movwf ADCON0        ;ADCS1=1 ADCS0=1; (Seleccion de frecuencias)
    					;CHS2=0 CHS1=0 CHS0=0 (Selección de pin de entrada)
                        ;GO/DONE=0 - ADON=1

MAIN:
    bsf ADCON0,2     ;Comienza la conversión
    call retardo     ;de tiempo para ejecutar la conversión
ESPERA: 
    btfsc ADCON0,2   ;Comprueba si ha terminado la conversión
    goto ESPERA      ;Si no, vuelve a comprobar
    movf ADRESH,0    ;Se carga el valor en digital
    movwf PORTB      ;Escribe el resultado de la conversión en el puerto B
    goto MAIN

retardo:
;Retardo de 20us
;34*3*200ns=20.4us
    movlw d'34'
    movwf valor1
loop
    decfsz valor1
    goto loop
    return
end