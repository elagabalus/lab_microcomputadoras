processor 16f877
include<p16f877.inc>

;Tiempo del retardo y direcciones de memoria
valor1 equ h'21'
valor2 equ h'22'
valor3 equ h'23'
ret1 equ d'13'
ret2 equ d'255'
ret3 equ d'255'

;Direccion de memoria auxiliar y cadenas de entrada
;Para el control de los motores
aux equ h'24'

c0 equ 0h
c1 equ 1h
c2 equ 2h
c4 equ 4h

org 0h
goto INICIO
org 05h

INICIO:
    ;CONFIGURACIÓN PUERTO B COMO SALIDA
    bsf STATUS,RP0   ;Se selecciona el banco 1, poniendo el bit 5 (RP0) en 1
    bcf STATUS,RP1   ;y poninendo el bit 6 (RP1) en 0
    movlw H'0'       ;Se carga un cero al registro de trabajo
    movwf TRISB      ;Se configura el puerto B como salida
    clrf PORTB       ;Se ponen en cero todos los bits del puerto B
    ;CONFIGURACIÓN PUERTO A COMO ENTRADA
    movlw 06h        ;Configura puertos A y E como digitales
    movwf ADCON1
    movlw 3fh        ;Configura el puerto A como entrada
    movwf TRISA
    clrf PORTA       ;Todos los bits del puerto A a 0

    bcf STATUS,RP0    ;Se vuelve a selecionar el banco 0
MAIN:
    ;CERO
    movlw c0        ;Se carga la cadena de bits para realizar CERO
    movwf aux       ;Se carga en una dirección de memoria auxiliar
    movfw PORTA     ;Se carga el valor de entrada en el puerto A
                    ;a la memoria de trabajo
    andlw b'000111' ;Enmascara la entrada leida para solo utiliza los
                    ;tres bits más significativos
    xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
    btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la función
    goto CERO       ;Se ejecuta la funcion que da la salida de CERO

    ;UNO
    movlw c1        ;Se carga la cadena de bits para realizar UNO
    movwf aux       ;Se carga en una dirección de memoria auxiliar
    movfw PORTA     ;Se carga el valor de entrada en el puerto A
                    ;a la memoria de trabajo
    andlw b'000111' ;Enmascara la entrada leida para solo utiliza los
                    ;tres bits más significativos
    xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
    btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la función
    goto UNO       ;Se ejecuta la funcion que da la salida de UNO

    ;DOS
    movlw c2        ;Se carga la cadena de bits para realizar DOS
    movwf aux       ;Se carga en una dirección de memoria auxiliar
    movfw PORTA     ;Se carga el valor de entrada en el puerto A
                    ;a la memoria de trabajo
    andlw b'000111' ;Enmascara la entrada leida para solo utiliza los
                    ;tres bits más significativos
    xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
    btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la función
    goto DOS       ;Se ejecuta la funcion que da la salida de DOS

    ;CUATRO
    movlw c4        ;Se carga la cadena de bits para realizar CUATRO
    movwf aux       ;Se carga en una dirección de memoria auxiliar
    movfw PORTA     ;Se carga el valor de entrada en el puerto A
                    ;a la memoria de trabajo
    andlw b'000111' ;Enmascara la entrada leida para solo utiliza los
                    ;tres bits más significativos
    xorwf aux,w     ;Comprueba si la entrada corresponde a la cadena
    btfsc STATUS,Z  ;Si la bandera Z es igual a 0 se ejecuta la función
    goto CUATRO       ;Se ejecuta la funcion que da la salida de CUATRO

CERO:
    movlw h'0000'
    movwf PORTB    ;MOTOR IZQ=PARO DER=PARO
    goto MAIN

UNO:
    movlw b'1111'
    movwf PORTB   ;MOTOR IZQ=ADELANTE DER=ATRAS
    call retardo
    goto MAIN

DOS:
    movlw b'1011'
    movwf PORTB   ;MOTOR IZQ=ADELANTE DER=ADELANTE
    call retardo
    goto MAIN

CUATRO:
    movlw b'1110'
    movwf PORTB    ;MOTOR IZQ=ATRAS DER=ADELANTE
    call retardo
    goto MAIN

retardo
    ;Rutina que genera un retardo
    movlw ret1
    movwf valor1
tres
    movwf valor2
    movwf ret2
dos
    movlw ret3
    movwf valor3
uno
    decfsz valor3
    goto uno
    decfsz valor2
    goto dos
    decfsz valor1
    goto tres
    return
end
